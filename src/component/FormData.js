import React from "react";

import { useForm } from "react-hook-form";
import "./Form.css";

function FormData() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    trigger,
    reset,
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
    reset(alert("Click here! to successfull your registration"));
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit(onSubmit)}>
        <h3 className="form-header"> Students Registation</h3>
        <label>FirstName</label>
        <input
          type="text"
          {...register("fname", {
            required: "fName cannot be empty",

            minLength: {
              value: 2,
              message: "maximum required length is less than 2",
            },
          })}
          onKeyUp={() => {
            trigger("fname");
          }}
        />
        {errors.fname && (
          <small className="error-message">{errors.fname.message}</small>
        )}

        {errors.firstName?.type === "required" && "First name is required"}
        <label>LastName</label>
        <input
          type="text"
          {...register("lname", {
            required: "lName cannot be empty",
            minLength: {
              value: 4,
              message: "maximum required length is less than 4",
            },
          })}
          onKeyUp={() => {
            trigger("lname");
          }}
        />
        {errors.lname && (
          <small className="error-message">{errors.lname.message}</small>
        )}

        {errors.firstName?.type === "required" && "Last name is required"}

        <label>Age</label>
        <input
          type="number"
          {...register("age", {
            required: "age is required",
            min: { value: 18, message: "minimum required age is 18" },
            max: { value: 99, message: "maximum required age is 99" },
            pattern: {
              value: /^[0-9]*$/,
              message: "only number are allowed",
            },
          })}
          onKeyUp={() => {
            trigger("age");
          }}
        />
        {errors.age && (
          <small className="error-message">{errors.age.message}</small>
        )}
        <label>Date of Birth</label>
        <input type="date" {...register("date", { required: true })} />
        {errors.date && (
          <small className="error-message">
            Your birthdate is required please add.
          </small>
        )}
        <label>Phone Number</label>
        <input
          type="number"
          {...register("number", {
            required: " please add your phone number it's required..",
            pattern: {
              value:
                /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
              message: "Invalid phone number..",
            },
          })}
          onKeyUp={() => {
            trigger("number");
          }}
        />
        {errors.number && (
          <small className="error-message">{errors.number.message}</small>
        )}
        <label>Current SEM</label>
        <select
          {...register("Field", {
            required: {
              value: true,
              message: "At least one sem should be selected.",
            },
          })}
        >
          <option value="">your sem..</option>
          <option value="SEM-2">SEM-2</option>
          <option value="SEM-4">SEM-4</option>
          <option value="SEM-8">SEM-8</option>
        </select>
        {errors.Field && (
          <small className="error-message">{errors.Field.message}</small>
        )}
        <label>Enrollment No.</label>
        <input
          type="number"
          {...register("enroll", {
            required: " please add your Enrollment number..",
            minLength: {
              value: 12,
              message: "your enrollment number is not valid..",
            },
          })}
          onKeyUp={() => {
            trigger("enroll");
          }}
        />
        {errors.enroll && (
          <small className="error-message">{errors.enroll.message}</small>
        )}

        <label>Email</label>
        <input
          type="email"
          {...register("email", {
            required: "Email cannot be empty.",
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
              message: "Invalid email address",
            },
          })}
          onKeyUp={() => {
            trigger("email");
          }}
        />
        {errors.email && (
          <small className="error-message">{errors.email.message}</small>
        )}
        <label>Password</label>
        <input
          type="password"
          {...register("password", {
            required: "Password cannot be empty",
            minLength: {
              value: 5,
              message:
                "minimum length 5,include at least one number or symbol.",
            },
            maxLength: {
              value: 10,
              message:
                "maximum length 10,include at least one number or symbol.",
            },
          })}
          onKeyUp={() => {
            trigger("password");
          }}
        />
        {errors.password && (
          <small className="error-message">{errors.password.message}</small>
        )}

        <label>select Field</label>
        <select
          {...register("Field", {
            required: {
              value: true,
              message: "At least one field should be selected.",
            },
          })}
        >
          <option value="">Select field..</option>
          <option value="Web Devlopement">Web Devlopement</option>
          <option value="QA">QA</option>
          <option value="Python">Python</option>
          <option value="IOS Devlopement">IOS Devlopement</option>
          <option value="others">others</option>
        </select>
        {errors.Field && (
          <small className="error-message">{errors.Field.message}</small>
        )}
        <label>Joining Date</label>
        <input type="date" {...register("date", { required: true })} />
        {errors.date && (
          <small className="error-message">
            Your birthdate is required please add.
          </small>
        )}
        <label>Gender</label>
        <div className="genders">
          <p className="male">
            male
            <input
              className="radio"
              type="radio"
              {...register("Gender", {
                required: { value: "male", message: "Please select any one.." },
              })}
            />
          </p>
          <p className="male">
            female
            <input
              className="radio"
              type="radio"
              {...register("Gender", {
                required: {
                  value: "female",
                  message: "Please select any one..",
                },
              })}
            />
          </p>
          <p className="male">
            other
            <input
              className="radio"
              type="radio"
              {...register("Gender", {
                required: {
                  value: "other",
                  message: "Please select any one..",
                },
              })}
            />
          </p>
          {errors.Gender && (
            <small className="error-message">{errors.Gender.message}</small>
          )}
        </div>
        <label>Message</label>
        <textarea
          type="text"
          className="message"
          {...register("mes", {
            required: "please add somthing it's required..",
            minLength: {
              value: 10,
              message: "minimum repuired length is greter than 10",
            },
            maxLength: {
              value: 50,
              message: "maximum required length is less than 50",
            },
          })}
          onKeyUp={() => {
            trigger("mes");
          }}
        />
        {errors.mes && (
          <small className="error-message">{errors.mes.message}</small>
        )}

        <br></br>
        <p className="check">
          <input
            type="checkbox"
            {...register("checkbox", {
              required: {
                value: true,
                message: "please check this box if you want to proceed.",
              },
            })}
          />
          yes,i would like to recieve my updates
        </p>
        {errors.checkbox && (
          <small className="error-message">{errors.checkbox.message}</small>
        )}

        <button type="submit">Create Your Account</button>
      </form>
    </div>
  );
}

export default FormData;
